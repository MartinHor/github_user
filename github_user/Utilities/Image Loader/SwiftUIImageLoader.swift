//
//  SwiftUIImageLoader.swift
//  github_user
//
//  Created by Martin on 28/12/2022.
//

import SwiftUI

final class SwiftUIImageLoader: ObservableObject {
    
    @Published var image: Image? = nil
    
    func load(fromURLString urlString: String) {
        ImageRequestManager.shared.downloadImage(fromURLString: urlString) { uiImage in
            guard let uiImage = uiImage else { return }
            DispatchQueue.main.async {
                self.image = Image(uiImage: uiImage)
            }
        }
    }
}

struct RemoteImage: View {
    
    var image: Image?
    
    var body: some View {
        // if we have an image return that otherwise use the placeholder
       
        if image != nil {
            image?.resizable()
        } else {
            LoadingView()
        }
    }
}

struct SwiftUIRemoteImage: View {
    
    // sees the change and redraws
    @StateObject var imageLoader = SwiftUIImageLoader()
    let urlString: String
    
    var body: some View {
        RemoteImage(image: imageLoader.image)
            .onAppear {
                imageLoader.load(fromURLString: urlString)
            }
    }
}
