//
//  UIKitImageLoader.swift
//  github_user
//
//  Created by Martin on 28/12/2022.
//

import UIKit

class UIKitRemoteImageView: UIImageView {
    
    let cache = ImageRequestManager.shared.cache
    let placeholderImage = UIImage(named: "img-placeholder")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        layer.cornerRadius  = 10
        clipsToBounds       = true
        image               = placeholderImage
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    func downloadImage(fromURL url: String) {
        ImageRequestManager.shared.downloadImage(fromURLString: url) { [weak self] (downloadedImage) in
            guard let self = self else { return }
            if url == "" {
                DispatchQueue.main.async {
                    self.image = self.placeholderImage
                }
            }
            guard let downloadedImage = downloadedImage else {
                DispatchQueue.main.async {
                    self.image = self.placeholderImage
                }
                return
            }
            DispatchQueue.main.async {
                self.image = downloadedImage
            }
        }
    }
}
