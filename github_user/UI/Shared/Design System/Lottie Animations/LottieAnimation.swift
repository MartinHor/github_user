import Foundation
import Lottie

class LottieAnimation {
    
    class var shimmerAnimation: LottieAnimationView {
        let animation = LottieAnimationView(name: "user_list_shimmer")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.backgroundBehavior = .pauseAndRestore
        animation.scalesLargeContentImage = true
        animation.play()
        return animation
    }
    
    class var loadingYellowDotsAnimation: LottieAnimationView {
        let animation = LottieAnimationView(name: "loading_yellow_dots")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.backgroundBehavior = .pauseAndRestore
        animation.play()
        return animation
    }
}
