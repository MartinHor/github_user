import UIKit

protocol StatefulPlaceholderViewDelegate: AnyObject {
    func statefulPlaceholderViewRetryButtonDidTap(_ statefulPlaceholderView: StatefulPlaceholderView)
}

// Placeholder view that binds `StatefulViewModelState`.
class StatefulPlaceholderView: UIView {
    //----------------------------------------
    // MARK: - Initialization
    //----------------------------------------

    override init(frame: CGRect) {
        super.init(frame: frame)

        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        sharedInit()
    }

    private func sharedInit() {
        let view = Bundle.main.loadNibNamed(String(describing: StatefulPlaceholderView.self),
                                            owner: self,
                                            options: nil)?.first as! UIView

        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(view.constraints(pinningEdgesTo: self))
        backgroundColor = .clear
                
        retryButton.cornerRadius = 4
        retryButton.layer.borderWidth = 1
        retryButton.layer.borderColor = UIColor(named: "borderColor")?.cgColor
    }
    
    //----------------------------------------
    // MARK: - Update Views
    //----------------------------------------
    
    private func startAnimation() {
        if isUsingShimmerAnimation {
            animationView = LottieAnimation.shimmerAnimation
            shimmerAnimationParentView.isHidden = false
            shimmerAnimationContainerView.addSubview(animationView)
            NSLayoutConstraint.activate(animationView.constraints(pinningEdgesTo: shimmerAnimationContainerView))
            animationView.play()
        } else {
            animationParentView.isHidden = false
            animationContainerView.addSubview(animationView)
            NSLayoutConstraint.activate(animationView.constraints(pinningEdgesTo: animationContainerView))
            animationView.play()
        }
    }
    
    private func stopAnimation() {
        if isUsingShimmerAnimation {
            shimmerAnimationParentView.isHidden = true
            animationView.stop()
            shimmerAnimationContainerView.subviews.forEach({ $0.removeFromSuperview() })
        } else {
            animationParentView.isHidden = true
            animationView.stop()
            animationContainerView.subviews.forEach({ $0.removeFromSuperview() })
        }
    }

    //----------------------------------------
    // MARK: - View bindings
    //----------------------------------------

    func bind<T>(_ state: State<T>) {
        switch state {
        case .loading:
            isHidden = false
            startAnimation()
            
            imageView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            retryButton.isHidden = true
            
        case .loadingFailed(let error):
            isHidden = false
            stopAnimation()
            
            imageView.isHidden = false
            titleLabel.isHidden = false
            subtitleLabel.isHidden = false
            retryButton.isHidden = false
            
            // Update view based on error.
            let error = error as? AppError
            if error == .network {
                imageView.image = UIImage(named: "img-no-internet-connection")
                titleLabel.text = "You're offline"
                subtitleLabel.text = "Check your internet and try again."
            } else {
                imageView.image = UIImage(named: "img-error")
                titleLabel.text = "Something went wrong"
                subtitleLabel.text = "Please try again later"
            }
            
        case .retryingLoad:
            isHidden = false
            startAnimation()
            
            imageView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            retryButton.isHidden = true
            
        case .loaded:
            isHidden = true
            stopAnimation()
            
            imageView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            retryButton.isHidden = true
        
        case .manualReloading:
            isHidden = true
            stopAnimation()

            imageView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            retryButton.isHidden = true
            
        case .manualReloadingFailed:
            isHidden = true
            stopAnimation()

            imageView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            retryButton.isHidden = true
        }
    }
    
    //----------------------------------------
    // MARK: - Actions
    //----------------------------------------
    
    @IBAction func retryButtonDidTap(_ sender: UIButton) {
        delegate?.statefulPlaceholderViewRetryButtonDidTap(self)
    }
    
    func setEmptyPlaceholder(withRetryOption: Bool = false) {
        isHidden = false
        stopAnimation()
        
        imageView.isHidden = false
        titleLabel.isHidden = false
        subtitleLabel.isHidden = !withRetryOption
        retryButton.isHidden = !withRetryOption
        
        imageView.image = UIImage(named: "img-empty")
        titleLabel.text = "Nothing Here Yet"
        subtitleLabel.text = "Please Try Again Later"
    }

    //----------------------------------------
    // MARK: - Delegate
    //----------------------------------------

    weak var delegate: StatefulPlaceholderViewDelegate?
    
    //----------------------------------------
    // MARK: - Animation View
    //----------------------------------------
    
    lazy var animationView = LottieAnimation.loadingYellowDotsAnimation
    
    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    var isUsingShimmerAnimation : Bool = false
    
    //----------------------------------------
    // MARK: - Outlets
    //----------------------------------------
    
    @IBOutlet private var imageView: UIImageView!
    
    @IBOutlet private var titleLabel: UILabel!
    
    @IBOutlet private var subtitleLabel: UILabel!
    
    @IBOutlet private var retryButton: UIButton!
    
    @IBOutlet private var animationContainerView: UIView!
    
    @IBOutlet private var animationParentView: UIView!
    
    @IBOutlet private var shimmerAnimationContainerView: UIView!
    
    @IBOutlet private var shimmerAnimationParentView: UIView!
}
