//
//  HomeViewController.swift
//  fladmin
//
//  Created by Martin on 23/12/2022.
//

import UIKit
import Combine
import MJRefresh

class HomeViewController: UIViewController {
    
    //----------------------------------------
    // MARK: - View model
    //----------------------------------------
    
    var viewModel: HomeViewModel = HomeViewModel()
    
    //----------------------------------------
    // MARK: - Lifecycle
    //----------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        bindViewModel()
    }
    
    //----------------------------------------
    // MARK: - Deinitialization
    //----------------------------------------
    
    deinit {
        cancellables.forEach { $0.cancel() }
    }
    
    //----------------------------------------
    // MARK: - Bind View Model
    //----------------------------------------
    
    private func bindViewModel() {
        viewModel.statePublisher
            .sink { [weak self] state in
                guard let self = self else { return }
                self.collectionView.mj_footer?.endRefreshing()
                self.statefulPlaceholderView.bind(state)
                switch state {
                case .loaded(let data):
                    if data.isEmpty {
                        self.statefulPlaceholderView.setEmptyPlaceholder(withRetryOption: true)
                        self.collectionView.mj_footer = nil
                    } else {
                        self.collectionView.mj_footer = self.footer
                    }
                default:
                    self.collectionView.mj_footer = nil
                }
            }.store(in: &cancellables)
        
        viewModel.githubUsersSubject
            .sink { [weak self] githubUsers in
                guard let self = self,
                      let githubUsers = githubUsers else { return }
                self.applySnapshot(githubUsers: githubUsers, animatingDifferences: false)
            }.store(in: &cancellables)
    }
    
    //----------------------------------------
    // MARK: - Configure Views
    //----------------------------------------
    
    private func configureViews() {
        title = "Github users"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        statefulPlaceholderView.isUsingShimmerAnimation = true
        statefulPlaceholderView.delegate = self
        
        footer = MJRefreshAutoNormalFooter(refreshingBlock: {
            self.footerLoad()
        })
        
        collectionView.collectionViewLayout = createCollectionViewLayout()
        collectionView.register(cellType: GithubUserCell.self)
        collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        collectionView.delegate = self
        collectionView.keyboardDismissMode = .onDrag
        
        
        let searchController = UISearchController()
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search for a username"
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
    }
    
    @objc func footerLoad(){
        viewModel.loadMore().sink { completion in
        } receiveValue: { data in
            self.collectionView.mj_footer?.endRefreshing()
        }.store(in: &cancellables)
    }
    
    //----------------------------------------
    // MARK: - UI collection view layout
    //----------------------------------------
    
    func createCollectionViewLayout() -> UICollectionViewLayout {
        let sectionProvider = { (sectionIndex: Int,
                                 layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            
            let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(100)))
            item.contentInsets.bottom = 6
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(1000)), subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = .init(top: 0, leading: 12, bottom: 0, trailing: 12)
            
            return section
        }
        
        return UICollectionViewCompositionalLayout(sectionProvider: sectionProvider)
    }
    
    //----------------------------------------
    // MARK: - UI collection view data source
    //----------------------------------------
    
    private func applySnapshot(githubUsers: [GithubUser], animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(githubUsers)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    private func createDataSource() -> DataSource {
        let dataSource = DataSource(
            collectionView: collectionView,
            cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GithubUserCell.reuseIdentifier, for: indexPath) as? GithubUserCell {
                    let viewModel = GithubUserCellViewModel(githubUser: item, row: indexPath.row)
                    cell.bindViewModel(viewModel)
                    return cell
                }
                return UICollectionViewCell()
            })
        return dataSource
    }
    
    private lazy var dataSource = createDataSource()
    
    //----------------------------------------
    // MARK: - Section
    //----------------------------------------
    
    enum Section {
        case main
    }
    
    //----------------------------------------
    // MARK: - Type aliases
    //----------------------------------------
    
    typealias DataSource = UICollectionViewDiffableDataSource<Section, GithubUser>
    
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, GithubUser>
    
    //----------------------------------------
    // MARK: - Outlets
    //----------------------------------------
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var statefulPlaceholderView: StatefulPlaceholderView!
    var footer : MJRefreshAutoNormalFooter?
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    
    private var cancellables: Set<AnyCancellable> = Set()
}

//----------------------------------------
// MARK: - UI collection view delegate
//----------------------------------------

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let githubUser = viewModel.githubUsersSubject.value?[indexPath.item], let endpoint = githubUser.url else { return }
        let vc = ProfileInfoViewController(endpoint: endpoint, githubUser: githubUser)
        vc.onSaveButtonTapAction = { [weak self] in
            guard let self = self else { return }
            self.viewModel.retryInitialLoad()
        }
        self.navigationController?.pushViewController(vc, animated: true)
        self.viewModel.updateProfileToSeenStatus(githubUser: githubUser)
    }
}

//----------------------------------------
// MARK: - Stateful placeholder view delegate
//----------------------------------------

extension HomeViewController: StatefulPlaceholderViewDelegate {
    func statefulPlaceholderViewRetryButtonDidTap(_ statefulPlaceholderView: StatefulPlaceholderView) {
        viewModel.retryInitialLoad()
    }
}

//----------------------------------------
// MARK: - SearchController delegate
//----------------------------------------

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.githubUsersSubject
            .sink { [weak self] githubUsers in
                guard let self = self,
                      let githubUsers = githubUsers else { return }
                guard let filter = searchController.searchBar.text, !filter.isEmpty else {
                    self.collectionView.mj_footer = githubUsers.isEmpty ? nil : self.footer
                    self.applySnapshot(githubUsers: githubUsers, animatingDifferences: false)
                    return
                }
                self.collectionView.mj_footer = nil
                let filteredGitHubUsers = githubUsers.filter {
                    if let notes = $0.notes {
                        return $0.username.lowercased().contains(filter.lowercased()) || (notes.lowercased().contains(filter.lowercased()))
                    } else {
                       return $0.username.lowercased().contains(filter.lowercased())
                    }
                }
                
                self.applySnapshot(githubUsers: filteredGitHubUsers, animatingDifferences: false)
            }.store(in: &cancellables)
    }
}
