import UIKit
import Combine

class GithubUserCell: UICollectionViewCell, NibReusable {
    //----------------------------------------
    // MARK: - View Model Binding
    //----------------------------------------
    
    func bindViewModel(_ viewModel: GithubUserCellViewModel) {
        usernameLabel.text = viewModel.username
        detailsLabel.text = "id: \(viewModel.id)"
        notesImageView.isHidden = (viewModel.notes == nil)
        contentView.alpha = viewModel.isSeen ? 0.6 : 1.0
        notesLabel.isHidden = viewModel.notes == nil
        notesLabel.text = "notes: \(viewModel.notes ?? "")"
        ImageRequestManager.shared.downloadImage(fromURLString: viewModel.avatarURL) { [weak self] (downloadedImage) in
            guard let self = self else { return }
            guard let downloadedImage = downloadedImage else {
                DispatchQueue.main.async {
                    self.profileImageView.image = self.placeholderImage
                }
                return
            }
            DispatchQueue.main.async {
                self.profileImageView.image = viewModel.shouldInvertedAvatar ? downloadedImage.inverseImage(cgResult: true) : downloadedImage
            }
        }
    }
    
    //----------------------------------------
    // MARK: - Lifecycle
    //----------------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = placeholderImage
        cancellables.removeAll()
    }

    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    let placeholderImage = UIImage(named: "img-placeholder")
    var cancellables: Set<AnyCancellable> = Set()
    
    //----------------------------------------
    // MARK: - Outlets
    //----------------------------------------

    @IBOutlet private weak var profileImageView: UIImageView!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var detailsLabel: UILabel!
    @IBOutlet private weak var notesLabel: UILabel!
    @IBOutlet private weak var notesImageView: UIImageView!
}
