//
//  GithubUserViewModel.swift
//  github_user
//
//  Created by Martin on 02/01/2023.
//

import Foundation

class GithubUserCellViewModel {
    //----------------------------------------
    // MARK: - Initialization
    //----------------------------------------
    
    init(githubUser: GithubUser, row: Int) {
        self.githubUser = githubUser
        self.row = row
    }
    
    //----------------------------------------
    // MARK: - Presentation
    //----------------------------------------
    
    var avatarURL: String {
        return githubUser.avatarUrl ?? ""
    }
    
    var username: String {
        return githubUser.username
    }
    
    var id: String {
        return "\(githubUser.id)"
    }
    
    var notes: String? {
        return githubUser.notes
    }
    
    var shouldInvertedAvatar: Bool {
        return row % 4 == 3
    }
    
    var isSeen: Bool {
        return githubUser.seen
    }
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    
    private let githubUser: GithubUser
    private let row: Int
}
