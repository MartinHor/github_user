import Foundation
import Combine

class HomeViewModel : StatefulViewModel<[GithubUser]> {
    
    //----------------------------------------
    // MARK: - Data loading
    //----------------------------------------
    
    override func load() -> AnyPublisher<[GithubUser], Error> {
        return githubStore.fetchGithubUsers().map { githubUsers in
            self.githubUsersSubject.send(githubUsers)
            return githubUsers
        }.eraseToAnyPublisher()
    }
    
    func loadMore() -> AnyPublisher<[GithubUser], Error> {
        return githubStore.fetchMoreGithubUsers().map { githubUsers in
            self.githubUsersSubject.send(githubUsers)
            return githubUsers
        }.eraseToAnyPublisher()
    }
    
    //----------------------------------------
    // MARK: - Actions
    //----------------------------------------
    
    func updateProfileToSeenStatus(githubUser: GithubUser) {
        if !githubUser.seen {
            githubStore.updateProfileToSeen(githubUser: githubUser)
            retryInitialLoad()
        }
    }
    
    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    let githubUsersSubject = CurrentValueSubject<[GithubUser]?, Never>(nil)
    let lastUserIdSubject = CurrentValueSubject<Int, Never>(0)
    
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    
    private let githubStore: GithubStore = GithubStore.shared
    private var cancellableSet: Set<AnyCancellable> = Set()
}
