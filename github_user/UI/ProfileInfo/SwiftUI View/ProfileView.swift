import SwiftUI

struct ProfileView: View {
    
    @ObservedObject var viewModel : ProfileViewModel
    
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        switch viewModel.state {
        case .loading:
            LoadingView()
        case .loaded(let githubProfile):
            Form {
                Section(header: HStack{
                    Spacer()
                    SwiftUIRemoteImage(urlString: githubProfile.avatarUrl ?? "")
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 120, height: 120)
                        .cornerRadius(60)
                    Spacer()
                }) {}

                Section(header: Text("Info")) {
                    HStack{
                        Text("Name")
                        Spacer()
                        Text(githubProfile.username ?? "-")
                            .opacity(0.6)
                    }
                    HStack{
                        Text("Company")
                        Spacer()
                        Text(githubProfile.company ?? "-")
                            .opacity(0.6)
                    }
                    HStack{
                        Text("Blog")
                        Spacer()
                        Text(githubProfile.blog ?? "-")
                            .opacity(0.6)
                    }
                }

                Section(header: Text("Notes")) {
                    TextField("Type something", text: $viewModel.notes)
                        .opacity(0.8)
                }

                Section(footer: HStack{
                    Spacer()
                    Button("Save") {
                        viewModel.saveButtonTapped()
                    }.font(.system(size: 17, weight: .regular, design: .default))
                    Spacer()
                }) { }
            }
        default:
            VStack{
                Text("Someting went wrong.").font(.system(size: 20, weight: .regular, design: .default))
                Spacer().frame(height: 20)
                Button("Retry") {
                    viewModel.retryInitialLoad()
                }.font(.system(size: 17, weight: .regular, design: .default))
            }
        }
    }
}
//
//struct ProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileView()
//    }
//}
