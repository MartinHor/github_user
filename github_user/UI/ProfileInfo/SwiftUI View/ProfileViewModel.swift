//
//  ProfileViewModel.swift
//  github_user
//
//  Created by Martin on 02/01/2023.
//

import Foundation
import Combine

class ProfileViewModel: StatefulViewModel<GithubProfile>, ObservableObject {
    //----------------------------------------
    // MARK: - Initialization
    //----------------------------------------
    
    init(endpoint: String, githubUser: GithubUser) {
        self.notes = githubUser.notes ?? ""
        self.endpoint = endpoint
        self.githubUser = githubUser
    }
    
    override func load() -> AnyPublisher<GithubProfile, Error> {
        
        statePublisher.sink { [weak self] state in
            guard let self = self else { return }
            self.state = state
        }.store(in: &cancellable)
        
        return githubStore
            .fetchGithubProfile(endpoint: self.endpoint)
            .eraseToAnyPublisher()
    }
    
    //----------------------------------------
    // MARK: - Action
    //----------------------------------------
    
    func saveButtonTapped() {
        githubStore.updateGithubUserNotes(githubUser: githubUser, newNotes: notes)
        onSaveButtonTapAction?()
    }
    
    //----------------------------------------
    // MARK: - Observation block
    //----------------------------------------
    
    var onSaveButtonTapAction: (() -> Void)?
    
    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    @Published var state: State<GithubProfile> = .loading
    @Published var notes: String = ""
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    
    private var endpoint: String
    private let githubStore: GithubStore = GithubStore.shared
    private let githubUser : GithubUser
    private var cancellable: Set<AnyCancellable> = Set()
}
