//
//  ProfileInfoViewController.swift
//  github_user
//
//  Created by Martin on 28/12/2022.
//

import UIKit
import SwiftUI

class ProfileInfoViewController: UIViewController {
    //----------------------------------------
    // MARK: - Initialization
    //----------------------------------------
    
    convenience init(endpoint: String, githubUser: GithubUser) {
        self.init()
        self.endpoint = endpoint
        self.githubUser = githubUser
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSwiftUIView()
    }
    
    func addSwiftUIView() {
        guard let githubUser = githubUser else { return }
        let viewModel = ProfileViewModel(endpoint: endpoint, githubUser: githubUser)
        viewModel.onSaveButtonTapAction = { [weak self] in
            guard let self = self else { return }
            self.onSaveButtonTapAction?()
            self.navigationController?.popViewController(animated: true)
        }
        let controller = UIHostingController(rootView: ProfileView(viewModel: viewModel))
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChild(controller)
        self.view.addSubview(controller.view)
        controller.didMove(toParent: self)
        NSLayoutConstraint.activate(controller.view.constraints(pinningEdgesTo: view))
    }
    
    //----------------------------------------
    // MARK: - Observation block
    //----------------------------------------
    
    var onSaveButtonTapAction: (() -> Void)?
    
    //----------------------------------------
    // MARK: - Internal
    //----------------------------------------
    
    private var endpoint: String = ""
    private var githubUser: GithubUser? = nil
}
