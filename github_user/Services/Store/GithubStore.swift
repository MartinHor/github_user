import Foundation
import CoreData
import Combine

class GithubStore {
    //----------------------------------------
    // MARK: - Singleton
    //----------------------------------------
    
    static let shared = GithubStore()
    
    //----------------------------------------
    // MARK: - Fetch contents
    //----------------------------------------
    
    func fetchGithubUserFromLocal() -> [GithubUser]? {
        guard let githubUsers = self.coreDataProvider.fetchGithubUsers(), !githubUsers.isEmpty else {
           return nil
        }
        return githubUsers
    }
    
    func fetchGithubUsers() -> AnyPublisher<[GithubUser], Error> {
        // if there is data from core data, use it first
        // if there is not data from core data, fetch from api
        // append the data from load more to core data
        
        // Fetch from coreData
        if let localData = fetchGithubUserFromLocal() {
            return Result.Publisher(localData).eraseToAnyPublisher()
        }
        
        // Fetch from api
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.userInfo[CodingUserInfoKey.context!] = backgroundContext
        
        // To test with empty data, kindly replace with this endpoint 😃
        // let endPoint = "https://63a69134f8f3f6d4ab0e0e26.mockapi.io/emptyData"
        
        let queryItems = [
            URLQueryItem(name: "since", value: "0"),
            URLQueryItem(name: "per_page", value: "30")
        ]
    
        let endPoint = "https://api.github.com/users"
        let publisher = networkRequestManager.apiRequest(.get, endPoint, queryItems: queryItems)
            .decode(type: [GithubUser].self, decoder: decoder)
            .receive(on: DispatchQueue.main)
            .flatMap({ [weak self] githubUsers -> AnyPublisher<[GithubUser], Error> in
                guard let self = self else { return Empty().eraseToAnyPublisher() }
                // Save the latest data into local storage using backgroundContext
                self.coreDataProvider.saveGithubUsers(githubUsers: githubUsers)
                return Result.Publisher(githubUsers).eraseToAnyPublisher()
            })
            .tryCatch { [weak self] error -> AnyPublisher<[GithubUser], Error> in
                guard let self = self else { return Empty().eraseToAnyPublisher() }
                // Fetch the old persisting data with local storage using mainContext
                guard let githubUsers = self.coreDataProvider.fetchGithubUsers(), !githubUsers.isEmpty else {
                    throw error
                }
                print("githubUsers = ", githubUsers)
                return Result.Publisher(githubUsers).eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
        
        return publisher
    }
    
    func fetchMoreGithubUsers() -> AnyPublisher<[GithubUser], Error> {
        guard let lastUserId: Int = fetchGithubUserFromLocal()?.last?.id else  { return Empty().eraseToAnyPublisher() }
        
        // Fetch from api
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.userInfo[CodingUserInfoKey.context!] = backgroundContext
        
        let queryItems = [
            URLQueryItem(name: "since", value: "\(lastUserId)"),
            URLQueryItem(name: "per_page", value: "30")
        ]
        
        let endPoint = "https://api.github.com/users"
        let publisher = networkRequestManager.apiRequest(.get, endPoint, queryItems: queryItems)
            .decode(type: [GithubUser].self, decoder: decoder)
            .receive(on: DispatchQueue.main)
            .flatMap({ [weak self] githubUsers -> AnyPublisher<[GithubUser], Error> in
                guard let self = self else { return Empty().eraseToAnyPublisher() }
                // Save the latest data into local storage using backgroundContext
                self.coreDataProvider.saveGithubUsers(githubUsers: githubUsers)
                guard let latestGithubUsers = self.fetchGithubUserFromLocal() else  { return Empty().eraseToAnyPublisher() }
                return Result.Publisher(latestGithubUsers).eraseToAnyPublisher()
            })
            .tryCatch { [weak self] error -> AnyPublisher<[GithubUser], Error> in
                guard let self = self else { return Empty().eraseToAnyPublisher() }
                // Fetch the old persisting data with local storage using mainContext
                guard let githubUsers = self.coreDataProvider.fetchGithubUsers(), !githubUsers.isEmpty else {
                    throw error
                }
                print("githubUsers = ", githubUsers)
                return Result.Publisher(githubUsers).eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
        
        return publisher
    }
    
    func fetchGithubProfile(endpoint: String) -> AnyPublisher<GithubProfile, Error> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        
        let publisher = networkRequestManager.apiRequest(.get, endpoint)
            .decode(type: GithubProfile.self, decoder: decoder)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
        
        return publisher
    }
    
    func updateGithubUserNotes(githubUser: GithubUser, newNotes: String) {
        coreDataProvider.updateGithubUserNotes(githubUser: githubUser, newNotes: newNotes)
    }
    
    func updateProfileToSeen(githubUser: GithubUser) {
        coreDataProvider.updateProfileToSeen(githubUser: githubUser)
    }
    
    //----------------------------------------
    // MARK: - Publishers
    //----------------------------------------
    
    //var refreshTrigger = PassthroughSubject<Void, Never>()
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    private let coreDataProvider: CoreDataProvider = CoreDataProvider(coreDataStack: CoreDataStack.shared)
    private let networkRequestManager: NetworkRequestManager = NetworkRequestManager.shared
    
    /// Returns managed object context associated with the main queue.
    private lazy var mainContext: NSManagedObjectContext = {
        return coreDataProvider.coreDataStack.mainContext
    }()
    
    /// Returns managed object context associated with the main queue.
    private lazy var backgroundContext: NSManagedObjectContext = {
        return coreDataProvider.coreDataStack.workerContext
    }()
}
