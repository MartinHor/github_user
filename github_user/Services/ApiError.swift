import Foundation

struct ApiError: Codable {
    let message: String
    let code: String
    let errors: [FieldError]
}

struct FieldError: Codable, Error {
    let field: String?
    let code: String
    let resource: String?
}
