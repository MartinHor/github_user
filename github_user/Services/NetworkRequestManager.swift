import Combine
import Foundation

class NetworkRequestManager {
    //----------------------------------------
    // MARK: - Singleton
    //----------------------------------------
    
    static let shared = NetworkRequestManager()
    
    //----------------------------------------
    // MARK: - API requests
    //----------------------------------------
    
    func apiRequest(
        _ method: URLRequest.HTTPMethod,
        _ path: String,
        queryItems: [URLQueryItem]? = nil,
        requestBody: Data? = nil
    ) -> AnyPublisher<Data, Error> {
        apiRequest(method, path, queryItems: queryItems, requestBody: requestBody, requiresAuthorization: false)
    }
    
    private func apiRequest(
        _ method: URLRequest.HTTPMethod,
        _ path: String,
        queryItems: [URLQueryItem]? = nil,
        requestBody: Data? = nil,
        requiresAuthorization: Bool
    ) -> AnyPublisher<Data, Error> {
        var apiURL: URL?
        
        var url = apiBaseURL.appendingPathComponent(path)
        
        if path.contains("https") == true || path.contains("http") == true,
           let httpUrl = URL(string: path) {
            url = httpUrl
        }
        
        guard var urlComponents = URLComponents(
            url: url,
            resolvingAgainstBaseURL: false
        ) else {
            return Fail(error: AppError.urlError).eraseToAnyPublisher()
        }
        
        // Append query items.
        urlComponents.queryItems = queryItems
        apiURL = urlComponents.url
        
        guard let url = apiURL else {
            return Fail(error: AppError.urlError).eraseToAnyPublisher()
        }
        print("url = ",url)
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = requestBody
        
        return urlSession.dataTaskPublisher(for: request)
            .tryMap { data, response in
                guard let httpURLResponse = response as? HTTPURLResponse,
                      (200 ... 299).contains(httpURLResponse.statusCode) else {
                    throw self.handleResponseFailure(data: data,
                                                     response: response)
                }
                return data
            }
            .mapError{ error in
                return self.handleRequestFailure(error: error)
            }
            .eraseToAnyPublisher()
    }
    
    
    //----------------------------------------
    // MARK: - Error handling
    //----------------------------------------
    
    private func handleRequestFailure(error: Error) -> AppError {
        var wrappedError = AppError.badRequest
        if (error as NSError).domain == NSURLErrorDomain {
            wrappedError = AppError.network
        }
        
        return wrappedError
    }
    
    private func handleResponseFailure(data: Data?, response: URLResponse?) -> AppError {
        var error = AppError.badRequest
        
        if let response = (response as? HTTPURLResponse) {
            switch response.statusCode {
            case 401:
                error = .authentication
                
                do {
                    let apiError = try JSONDecoder().decode(ApiError.self, from: data!)
                    switch apiError.code {
                        
                    default:
                        error = .authentication
                    }
                } catch {
                    break
                }
                
            default:
                error = .notFound
            }
        }
        
        return error
    }
    
    //----------------------------------------
    // MARK: - Internals
    //----------------------------------------
    
    private let apiBaseURL : URL = URL(string: "https://api.github.com/")!
    
    private lazy var urlSession: URLSession = {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 10.0
        sessionConfig.timeoutIntervalForResource = 10.0
        let session = URLSession(configuration: sessionConfig)
        return session
    }()
    
    private let _isConnectedToNetwork = CurrentValueSubject<Bool?, Never>(nil)
    
    private var cancellables: Set<AnyCancellable> = Set()
}
