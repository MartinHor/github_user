import Foundation

public enum AppError: Error, Equatable {
    case dataSerialization(reason: String?)
    case invalidData
    case urlError
    case network
    
    // HTTP Status Code 400 range.
    case authentication
    case badRequest
    case notFound
    
    // HTTP Status code 500 range.
    case serverError
}

enum DecoderConfigurationError: Error {
  case missingManagedObjectContext
}
