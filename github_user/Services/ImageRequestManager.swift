import Combine
import UIKit
import Foundation

class ImageRequestManager {
    //----------------------------------------
    // MARK: - Singleton
    //----------------------------------------
    
    static let shared = ImageRequestManager()
    
    
    //----------------------------------------
    // MARK: - Download image
    //----------------------------------------

    func downloadImage(fromURLString urlString: String, completed: @escaping (UIImage?) -> Void) {
        let cacheKey = NSString(string: urlString)
        if let image = cache.object(forKey: cacheKey) {
            completed(image)
            return
        }
        
        guard let url = URL(string: urlString) else {
            completed(nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in
            guard let data = data, let image = UIImage(data: data) else {
                completed(nil)
                return
            }
            self.cache.setObject(image, forKey: cacheKey)
            completed(image)
        }
        
        task.resume()
    }
    
    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    let cache = NSCache<NSString,UIImage>()
}
