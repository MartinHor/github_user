import Foundation
import CoreData

@objc(GithubUser)
public class GithubUser: NSManagedObject, Managed, Decodable {
    
    static var entityName: String { return "GithubUser" }
    
    //----------------------------------------
    // MARK: - Properties
    //----------------------------------------
    
    @NSManaged public var avatarUrl: String?
    @NSManaged public var username: String
    @NSManaged public var isSiteAdmin: Bool
    @NSManaged public var url: String?
    @NSManaged public var id: Int
    @NSManaged public var notes: String?
    @NSManaged public var seen: Bool
    
    //----------------------------------------
    // MARK: - Initialization
    //----------------------------------------
    
    // 'required' is needed for Decodable conformance.
    // 'convenience' to call self.init(entity:, insertInto).
    required convenience public init(from decoder: Decoder) throws {
        // Return the context from the decoder userinfo dictionary.
        guard let contextUserInfoKey = CodingUserInfoKey.context,
              let managedObjectContext = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
              let entity = NSEntityDescription.entity(forEntityName: GithubUser.entityName, in: managedObjectContext) else {
            
            throw DecoderConfigurationError.missingManagedObjectContext
        }
        
        // Super init of the NSManagedObject.
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        avatarUrl = try container.decodeIfPresent(String.self, forKey: .avatarUrl) ?? ""
        username = try container.decodeIfPresent(String.self, forKey: .username) ?? ""
        isSiteAdmin = try container.decodeIfPresent(Bool.self, forKey: .isSiteAdmin) ?? false
        url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        id = try container.decode(Int.self, forKey: .id)
        seen = false
    }
    
    //----------------------------------------
    // MARK: - Coding keys
    //----------------------------------------
    
    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case username = "login"
        case isSiteAdmin = "site_admin"
        case url = "url"
        case id = "id"
    }
}
