//
//  GithubProfile.swift
//  github_user
//
//  Created by Martin on 02/01/2023.
//

import Foundation

class GithubProfile: Decodable {
    let avatarUrl: String?
    let username: String?
    let company: String?
    let blog: String?
    let id: Int?
    
    //----------------------------------------
    // MARK: - Coding keys
    //----------------------------------------
    
    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case username = "login"
        case company = "company"
        case blog = "blog"
        case id = "id"
    }
    
    //----------------------------------------
    // MARK: - Decodable requirements
    //----------------------------------------
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        avatarUrl = try container.decodeIfPresent(String.self, forKey: .avatarUrl)
        username = try container.decodeIfPresent(String.self, forKey: .username)
        company = try container.decodeIfPresent(String.self, forKey: .company)
        blog = try container.decodeIfPresent(String.self, forKey: .blog)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
    }
}
