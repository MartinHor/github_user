import Foundation
import CoreData

/// A class defining and managing the core data stack.
class CoreDataStack {
    //----------------------------------------
    // MARK: - Singleton
    //----------------------------------------
   
    static let shared = CoreDataStack()
    
    // MARK: - Properties
    
    public static let modelName = "github_user"
    
    public static let model: NSManagedObjectModel = {
      // swiftlint:disable force_unwrapping
      let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd")!
      return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    /// Use this property to access managed object context associated with the main queue.
    lazy var mainContext: NSManagedObjectContext = {
        let context = persistentContainer.viewContext
        context.automaticallyMergesChangesFromParent = true
        
        return persistentContainer.viewContext
    }()

    /// Creates and configures a private queue context.
    lazy var workerContext: NSManagedObjectContext = {
        let context = persistentContainer.newBackgroundContext()

        context.automaticallyMergesChangesFromParent = true
        context.mergePolicy = AppMergePolicy(mode: .remote)

        return context
    }()

    // We should always make use of `viewContext` for main queue context
    // and `backgroundContext` for private queue context only.
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: CoreDataStack.modelName, managedObjectModel: CoreDataStack.model)
        container.loadPersistentStores { _, error in
            guard error == nil else {
                // Crash application if core data stack failed to load.
                fatalError("Failed to load store: \(error!)")
            }
        }

        return container
    }()
}
