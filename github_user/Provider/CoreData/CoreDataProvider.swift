//
//  CoreDataProvider.swift
//  github_user
//
//  Created by Martin on 02/01/2023.
//

import Foundation
import CoreData

class CoreDataProvider {
    //----------------------------------------
    // MARK: - Initializers
    //----------------------------------------
    
    public init(coreDataStack: CoreDataStack) {
      self._coreDataStack = coreDataStack
    }
    
    //----------------------------------------
    // MARK: - Operation
    //----------------------------------------
    
    func fetchGithubUsers() -> [GithubUser]? {
        var result: [GithubUser]?
        let context: NSManagedObjectContext = coreDataStack.workerContext
        
        context.performAndWait {
            do {
                let fetchRequest = NSFetchRequest<GithubUser>(entityName: GithubUser.entityName)
                let sort = NSSortDescriptor(key: #keyPath(GithubUser.id), ascending: true)
                fetchRequest.sortDescriptors = [sort]
                result = try context.fetch(fetchRequest)
            } catch {
                // Ignore.
            }
        }
        return result
    }
    
    func saveGithubUsers(githubUsers: [GithubUser]) {
        let context: NSManagedObjectContext = coreDataStack.workerContext
        do {
            try context.saveIfNeeded()
        } catch {
            context.rollback()
        }
    }
    
    func updateGithubUserNotes(githubUser: GithubUser, newNotes: String) {
        let context: NSManagedObjectContext = coreDataStack.workerContext
        if newNotes == "" {
            githubUser.notes = nil
        } else {
            githubUser.notes = newNotes
        }
        
        do {
            try context.saveIfNeeded()
        } catch {
            context.rollback()
        }
    }
    
    func updateProfileToSeen(githubUser: GithubUser) {
        let context: NSManagedObjectContext = coreDataStack.workerContext
        githubUser.seen = true
        do {
            try context.saveIfNeeded()
        } catch {
            context.rollback()
        }
    }
    
    //----------------------------------------
    // MARK: - Services
    //----------------------------------------
    
    private var _coreDataStack: CoreDataStack?
    var coreDataStack: CoreDataStack {
        if _coreDataStack == nil {
            _coreDataStack = CoreDataStack.shared
        }
        return _coreDataStack!
    }
}
