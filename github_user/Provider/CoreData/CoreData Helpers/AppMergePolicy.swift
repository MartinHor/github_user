import Foundation
import CoreData

/// A helper class to resolve conflicts between the persistent store and in-memory versions of managed objects.
final class AppMergePolicy: NSMergePolicy {
    // MARK: - Initialization

    init(mode: MergeMode) {
        super.init(merge: mode.mergeType)
    }

    override func resolve(optimisticLockingConflicts list: [NSMergeConflict]) throws {
        // Resolves the conflicts in a given list.
    }

    // MARK: - Configuration

    enum MergeMode {
        case remote
        case local

        var mergeType: NSMergePolicyType {
            switch self {
            case .remote: return .mergeByPropertyObjectTrumpMergePolicyType
            case .local: return .mergeByPropertyStoreTrumpMergePolicyType
            }
        }
    }
}

extension Sequence where Iterator.Element == NSMergeConflict {
    func conflictedObjects<T>(of cls: T.Type) -> [T] {
        let objects = map { $0.sourceObject }
        return objects.compactMap { $0 as? T }
    }

    func conflictsAndObjects<T>(of cls: T.Type) -> [(NSMergeConflict, T)] {
        return filter { $0.sourceObject is T }.map { ($0, $0.sourceObject as! T) }
    }
}
