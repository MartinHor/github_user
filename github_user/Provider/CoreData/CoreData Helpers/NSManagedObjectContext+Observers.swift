import Foundation
import CoreData

struct ContextDidSaveNotification {
    // MARK: - Initialization

    init(note: Notification) {
        guard note.name == .NSManagedObjectContextDidSave else { fatalError() }
        notification = note
    }

    // MARK: - Properties

    /// Use this property to obtain a set of objects that were inserted into the context.
    var insertedObjects: AnyIterator<NSManagedObject> {
        return iterator(forKey: NSInsertedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were updated into the context.
    var updatedObjects: AnyIterator<NSManagedObject> {
        return iterator(forKey: NSUpdatedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were updated into the context.
    var deletedObjects: AnyIterator<NSManagedObject> {
        return iterator(forKey: NSDeletedObjectsKey)
    }

    /// Use this property to obtain the managed object context for the object that is observed.
    var managedObjectContext: NSManagedObjectContext {
        guard let context = notification.object as? NSManagedObjectContext else {
            fatalError("Invalid notification object")
        }
        return context
    }

    // MARK: - Internals

    fileprivate let notification: Notification

    fileprivate func iterator(forKey key: String) -> AnyIterator<NSManagedObject> {
        guard let set = (notification as Notification).userInfo?[key] as? NSSet else {
            return AnyIterator { nil }
        }

        var innerIterator = set.makeIterator()
        return AnyIterator {
            return innerIterator.next() as? NSManagedObject
        }
    }
}

// MARK: - Custom debug string convertible

extension ContextDidSaveNotification: CustomDebugStringConvertible {
    public var debugDescription: String {
        var components = [notification.name.rawValue]
        components.append(managedObjectContext.description)
        let changedObjects = [
            ("inserted", insertedObjects),
            ("updated", updatedObjects),
            ("deleted", deletedObjects)
        ]

        for (name, set) in changedObjects {
            let all = set.map { $0.objectID.description }.joined(separator: ", ")
            components.append("\(name): {\(all)})")
        }

        return components.joined(separator: " ")
    }
}

struct ContextWillSaveNotification {
    // MARK: - Initialization

    init(note: Notification) {
        assert(note.name == .NSManagedObjectContextWillSave)
        notification = note
    }

    // MARK: - Properties

    var managedObjectContext: NSManagedObjectContext {
        guard let context = notification.object as? NSManagedObjectContext else {
            fatalError("Invalid notification object")
        }

        return context
    }

    // MARK: - Internals

    fileprivate let notification: Notification
}

public struct ObjectsDidChangeNotification {
    // MARK: - Initialization

    init(note: Notification) {
        assert(note.name == .NSManagedObjectContextObjectsDidChange)
        notification = note
    }

    // MARK: - Properties

    /// Use this property to obtain a set of objects that were inserted into the context.
    public var insertedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInsertedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were updated into the context.
    public var updatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSUpdatedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were deleted into the context.
    public var deletedObjects: Set<NSManagedObject> {
        return objects(forKey: NSDeletedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were refreshed into the context.
    public var refreshedObjects: Set<NSManagedObject> {
        return objects(forKey: NSRefreshedObjectsKey)
    }

    /// Use this property to obtain a set of objects that were invalidated into the context.
    public var invalidatedObjects: Set<NSManagedObject> {
        return objects(forKey: NSInvalidatedObjectsKey)
    }

    public var invalidatedAllObjects: Bool {
        return (notification as Notification).userInfo?[NSInvalidatedAllObjectsKey] != nil
    }

    /// Use this property to obtain the managed object context for the object that is observed.
    public var managedObjectContext: NSManagedObjectContext {
        guard let context = notification.object as? NSManagedObjectContext else {
            fatalError("Invalid notification object")
        }

        return context
    }

    // MARK: - Internals

    fileprivate let notification: Notification

    fileprivate func objects(forKey key: String) -> Set<NSManagedObject> {
        return ((notification as Notification).userInfo?[key] as? Set<NSManagedObject>) ?? Set()
    }
}

extension NSManagedObjectContext {

    /// Adds the given block to the default `NotificationCenter`'s dispatch table for the given context's did-save notifications.
    /// - returns: An opaque object to act as the observer. This must be sent to the default `NotificationCenter`'s `removeObserver()`.
    func addContextDidSaveNotificationObserver(
        _ completionHandler: @escaping (ContextDidSaveNotification) -> Void
    ) -> NSObjectProtocol {
        let notification = NotificationCenter.default
        return notification.addObserver(forName: .NSManagedObjectContextDidSave, object: self, queue: nil) { note in
            let wrappedNote = ContextDidSaveNotification(note: note)
            completionHandler(wrappedNote)
        }
    }

    /// Adds the given block to the default `NotificationCenter`'s dispatch table for the given context's will-save notifications.
    /// - returns: An opaque object to act as the observer. This must be sent to the default `NotificationCenter`'s `removeObserver()`.
    func addContextWillSaveNotificationObserver(
        _ completionHandler: @escaping (ContextWillSaveNotification) -> Void
    ) -> NSObjectProtocol {
        let notification = NotificationCenter.default
        return notification.addObserver(forName: .NSManagedObjectContextWillSave, object: self, queue: nil) { note in
            let wrappedNote = ContextWillSaveNotification(note: note)
            completionHandler(wrappedNote)
        }
    }

    /// Adds the given block to the default `NotificationCenter`'s dispatch table for the given context's objects-did-change notifications.
    /// - returns: An opaque object to act as the observer. This must be sent to the default `NotificationCenter`'s `removeObserver()`.
    func addObjectsDidChangeNotificationObserver(_ completionHandler: @escaping (ObjectsDidChangeNotification) -> Void) -> NSObjectProtocol {
        let notification = NotificationCenter.default
        return notification.addObserver(forName: .NSManagedObjectContextObjectsDidChange, object: self, queue: nil) { note in
            let wrappedNote = ObjectsDidChangeNotification(note: note)
            completionHandler(wrappedNote)
        }
    }

    /// Merging a did-save notification into a context will refresh the registered objects that have been changed,
    /// remove the ones that have been deleted, and fault in the ones that have been newly inserted.
    func performMergeChanges(from note: ContextDidSaveNotification) {
        perform {
            self.mergeChanges(fromContextDidSave: note.notification)
        }
    }
}

extension NSManagedObjectContext {

    /// Only performs a save if there are changes to commit.
    /// - Returns: `true` if a save was needed. Otherwise, `false`.
    @discardableResult public func saveIfNeeded() throws -> Bool {
        guard hasChanges else { return false }
        try save()
        return true
    }
}
