//
//  CoreDataProviderTests.swift
//  github_userTests
//
//  Created by Martin on 04/01/2023.
//

import XCTest
import CoreData
@testable import github_user

final class CoreDataProviderTests: XCTestCase {
    
    // MARK: - Properties
    var coreDataProvider: CoreDataProvider!
    var coreDataStack: CoreDataStack!
    
    override func setUp() {
        coreDataStack = TestCoreDataStack()
        coreDataProvider = CoreDataProvider(coreDataStack: coreDataStack)
    }
    
    override func tearDown() {
        super.tearDown()
        coreDataProvider = nil
        coreDataStack = nil
    }
    
    func testFetchGithubUsers() {
        let githubUser = GithubUser(context: coreDataStack.mainContext)
        githubUser.id = 0
        githubUser.username = "Martin"
        githubUser.url = "https://api.github.com/users/elbowdonkey"
        githubUser.notes = "Aloha"
        githubUser.avatarUrl = "https://avatars.githubusercontent.com/u/51?v=4"
        githubUser.isSiteAdmin = true
        
        coreDataProvider.saveGithubUsers(githubUsers: [githubUser])
        
        let getGithubUsers = coreDataProvider.fetchGithubUsers()

        XCTAssertNotNil(getGithubUsers)
        XCTAssertTrue(getGithubUsers?.count == 1)
        XCTAssertTrue(githubUser.id == getGithubUsers?.first?.id)
    }
    
    func testSaveGithubUsers() {
        let githubUser1 = GithubUser(context: coreDataStack.mainContext)
        githubUser1.id = 1
        githubUser1.username = "Martin 1"
        githubUser1.url = "https://api.github.com/users/elbowdonkey"
        githubUser1.notes = "Aloha 1"
        githubUser1.avatarUrl = "https://avatars.githubusercontent.com/u/51?v=4"
        githubUser1.isSiteAdmin = false
        
        let githubUser2 = GithubUser(context: coreDataStack.mainContext)
        githubUser2.id = 2
        githubUser2.username = "Martin 2"
        githubUser2.url = "https://api.github.com/users/elbowdonkey"
        githubUser2.notes = "Aloha 2"
        githubUser2.avatarUrl = "https://avatars.githubusercontent.com/u/51?v=4"
        githubUser2.isSiteAdmin = false
        
        coreDataProvider.saveGithubUsers(githubUsers: [githubUser1, githubUser2])
        
        let getGithubUsers = coreDataProvider.fetchGithubUsers()
        
        XCTAssertNotNil(getGithubUsers)
        XCTAssertTrue(getGithubUsers?.count == 2)
        XCTAssertTrue(githubUser1.id == getGithubUsers?.first?.id)
    }
    
    func testUpdateNotes() {
        let githubUser = GithubUser(context: coreDataStack.mainContext)
        githubUser.id = 0
        githubUser.username = "Martin"
        githubUser.url = "https://api.github.com/users/elbowdonkey"
        githubUser.notes = "Sample notes"
        githubUser.avatarUrl = "https://avatars.githubusercontent.com/u/51?v=4"
        githubUser.isSiteAdmin = false
        
        coreDataProvider.saveGithubUsers(githubUsers: [githubUser])
        var getGithubUsers = coreDataProvider.fetchGithubUsers()

        XCTAssertNotNil(getGithubUsers)
        XCTAssertTrue(getGithubUsers?.count == 1)
        XCTAssertTrue(getGithubUsers?.first?.notes == "Sample notes")
        
        coreDataProvider.updateGithubUserNotes(githubUser: githubUser, newNotes: "New notes")
        
        getGithubUsers = coreDataProvider.fetchGithubUsers()
        XCTAssertNotNil(getGithubUsers)
        XCTAssertTrue(getGithubUsers?.count == 1)
        XCTAssertTrue(getGithubUsers?.first?.notes == "New notes")
    }
}
